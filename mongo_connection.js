const mongoose = require("mongoose");

module.exports = class Conexion {
  constructor() {

  }

  async IniciarConexion(){
    mongoose.connect(process.env.MONGO_URI);
  }

  async LimpiarDatos() {
    const collections = mongoose.connection.collections;
    await Promise.all(
      Object.values(collections).map(async (collection) => {
        await collection.deleteMany({});
      })
    );
    mongoose.connection.close();
    console.log("Conexion a base de datos cerrada y limpiada")
  }

  
}

mongoose.connection.on("connected", function () {
  console.log("Conexion a base de datos exitosa");
});

mongoose.connection.on("error", function (ex) {
  console.log("error:" + ex);
});


