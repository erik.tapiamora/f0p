//PRINCIPALES
const Conexion = require("./mongo_connection");
//REQUIRES
require("dotenv").config();
//GENERACION DE REGISTROS
const GenerarUsuarios = require("./database/GenerarUsuarios");
const GenerarSucursales = require("./database/GenerarSucursales");
const GenerarCategorias = require("./database/GenerarCategorias");
const GenerarMovimientos = require("./database/GenerarMovimientos");
const GenerarLibro = require("./database/GenerarLibro");
const GenerarRelacionesLibrosSucursales = require("./database/GenerarRelacionesLibrosSucursales");
//PRUEBAS
const Test_Login = require("./test/1_login.test");
const Test_Listado_Sucursales = require("./test/2_listadoSucursal.test");
const Test_Alta_Sucursal= require("./test/3_altaSucursal.test");
const Test_Baja_Sucursal= require("./test/4_bajaSucursal.test");
const Test_Modificacion_Sucursal = require("./test/5_modificacionSucursal.test");
const Test_Listado_Categoria = require("./test/6_listadoCategoria.test");
const Test_Alta_Categoria= require("./test/7_altaCategoria.test");
const Test_Baja_Categoria= require("./test/8_bajaCategoria.test");
const Test_Modificacion_Categoria = require("./test/9_modificacionCategoria.test");
const Test_Listado_Inventario = require("./test/10_listadoInventario.test");
const Test_Modificacion_Inventario = require("./test/11_modificacionInventario.test");
const Test_Busqueda_Libro = require("./test/12_busquedaLibro.test");
const Test_Listado_Libro = require("./test/13_listadoLibro.test");
const Test_Alta_Libro = require("./test/14_altaLibro.test");
const Test_Baja_Libro = require("./test/15_bajaLibro.test");
const Test_Modificacion_Libro = require("./test/16_modificacionLibro.test");
const Test_Venta = require("./test/17_GeneracionVenta.test");
const Test_Reporte = require("./test/18_reporteVentas.test");

describe("Prueba de funcionamiento de libreria", function () {
  //CREACION DE UN OBJECTO BASADO EN CLASE PARA LA CREACION DE LA CONEXION
  let conexion = new Conexion();
  //FUNCION BEFORE DONDE SE INICIALIZA LA CONEXION A LA BASE DE DATOS
  //Y SE CREAN REGISTROS PARA LA REALIZACION DE LAS PRUEBAS
  before(async function () {
    await conexion.IniciarConexion();
    await GenerarUsuarios();
    await GenerarSucursales();
    await GenerarCategorias();
    await GenerarLibro();
    await GenerarMovimientos();
    await GenerarRelacionesLibrosSucursales();
  });
  ///FUNCION AFTER QUE LIMPIA LA BASE DE DATOS AL TERMINAR LAS PRUEBAS
  after(async function () {
    conexion.LimpiarDatos();
  });
  //INVOCACION DE PRUEBAS
  Test_Login();
  Test_Listado_Sucursales();
  Test_Alta_Sucursal();
  Test_Baja_Sucursal();
  Test_Modificacion_Sucursal();
  Test_Listado_Categoria();
  Test_Alta_Categoria();
  Test_Baja_Categoria();
  Test_Modificacion_Categoria();
  Test_Listado_Inventario();
  Test_Modificacion_Inventario();
  Test_Busqueda_Libro();
  Test_Listado_Libro();
  Test_Alta_Libro();
  Test_Baja_Libro();
  Test_Modificacion_Libro();
  Test_Venta();
  Test_Reporte();
});
