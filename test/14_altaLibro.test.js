var assert = require("assert");
const Libro = require("../model/Libro");



module.exports= function (){
    describe('14 - Proceso de alta de nuevo libro',async function(){
        it('Se da de alta la categoria y se valida existe el registro',async function(){
            const nuevoRegistro = await Libro.create({
                id:10,
                titulo: 'Libro de estudios 5',
                autor: 'Erik Tapia',
                categoria_id: 1,
                codigo_interno: 'R001',
                editorial: 'Cesun'
            })
            const existeRegistro = await Libro.findOne({_id:nuevoRegistro._id});
            assert.equal(nuevoRegistro.id,existeRegistro.id);
        })

        it('Se verifica sean ahora 10 libros',async function(){

            const listadoRegistro = await Libro.find();
            assert.equal(listadoRegistro.length,10);
        })
    });
}