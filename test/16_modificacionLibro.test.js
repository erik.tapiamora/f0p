var assert = require("assert");
const Libro = require("../model/Libro");

module.exports= function (){
    describe('16 - Proceso de modificacion de libro',async function(){
        it('Se obtiene el registro, edita y valida se actualizará en DB',async function(){
            const nuevoValor = 'Historias Fantasticas';
            const registroAModificar = await Libro.findOneAndUpdate({
                id:1
            },
            {
                titulo: nuevoValor,
            });
            const registroEditado = await Libro.findOne({
                id:1
            });
            assert.equal(registroEditado.titulo,nuevoValor);
        })
    });
}