var assert = require("assert");
const Sucursal = require("../model/Sucursal");

module.exports= function (){
    describe('5 - Proceso de modificacion de sucursal',async function(){
        it('Se obtiene el registro, edita y valida se actualizará en DB',async function(){
            const nuevoValor = 'Libreria El Refugio';
            const registroAModificar = await Sucursal.findOneAndUpdate({
                id:3
            },
            {
                nombre: nuevoValor,
            });
            const registroEditado = await Sucursal.findOne({
                id:3
            });
            assert.equal(registroEditado.nombre,nuevoValor);
        })
    });
}