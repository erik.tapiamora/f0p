var assert = require("assert");
const Libro = require("../model/Libro");

module.exports= function (){
    describe('15 - Proceso de baja de libro',async function(){
        it('Se da de baja el libro y se valida existe el registro',async function(){
            const registroAEliminar = await Libro.findOne({
                id:10
            });
            await registroAEliminar.remove();
            const existeRegistro = await Libro.findOne({id:10});
            assert.equal(existeRegistro,null);
        })

        it('Se verifica sean ahora 9 libros',async function(){

            const listadoRegistro = await Libro.find();
            assert.equal(listadoRegistro.length,9);
        })
    });
}