var assert = require("assert");
const Categoria = require("../model/Categoria");

module.exports= function (){
    describe('6 - Ver listado de categorias',async function(){
        it('Obtiene un listado de categorias',async function(){
            let categorias = await Categoria.find();
            if(categorias){
                assert(true);
            }
            else{
                assert.fail('No existe un listado de categorias');
            }
        });

        it('Valida que el total de categorias existentes es 3',async function(){
            let categorias = await Categoria.find();
            assert.equal(categorias.length,3);            
        });
    });
}