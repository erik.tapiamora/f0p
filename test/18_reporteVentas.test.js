var assert = require("assert");
const Movimiento = require('../model/Movimiento')
module.exports= function (){
    describe('18 - Reporte de venta',async function(){
        it('Se genera un reporte global de movimientos',async function(){
            let reportes = await Movimiento.find();
            assert.equal(reportes.length>0,true);
        })

        it('Se genera un reporte de movimientos de una sucursal',async function(){
            let reportes = await Movimiento.find({sucursal_Id: 2});
            assert.equal(reportes.length>0,true);
        })

        it('Se genera un reporte de ventas globales',async function(){
            let reportes = await Movimiento.find({tipo: "Venta"});
            assert.equal(reportes.length>0,true);
        })

    });
}