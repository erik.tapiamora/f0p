var assert = require("assert");
const Libro = require("../model/Libro");
const Relacion_Libro_Sucursal = require("../model/Relacion_Libro_Sucursal");
const Movimiento = require('../model/Movimiento')
module.exports= function (){
    describe('17 - Generacion de venta',async function(){
        it('Se recibe un libro en la caja y se genera una venta',async function(){
            let libro = await Libro.findOne({id: 1});
            const fechaVenta = new Date();
            const nuevoMovimiento = await Movimiento.create({
                'cantidad': 1,
                'fecha':fechaVenta,
                'libro_Id' : libro.id,
                'sucursal_Id': 2,
                'tipo': 'Venta',
                'usuario_Id': 3 
            });
        
            assert.equal(fechaVenta,nuevoMovimiento.fecha);
        })

        it('Se modifica el inventario para marcar que no se cuenta ya con el libro',async function(){
            let libro = await Libro.findOne({id: 1});
            let sucursal_inventario = await Relacion_Libro_Sucursal.findOneAndUpdate(
                {
                    libro_id:libro.id,
                    sucursal_id:2
                },
                {
                    cantidad: 6
                });
            sucursal_inventario = await Relacion_Libro_Sucursal.findOne(
                {
                    libro_id: libro.id,
                    sucursal_id: 2
                }
            )
           
            assert.equal(sucursal_inventario.cantidad,6);
        });

    });
}