var assert = require("assert");
const Usuario = require("../model/Usuario");


module.exports= function (){
    describe('1 - Prueba de inicio de sesión',async function(){        
        it('Inicio de sesion correcto',async function(){
            let usuario = "Administrador";
            let password = "123456";
            const existeUsuario = await Usuario.findOne({usuario: usuario,password: password});
            if(existeUsuario){
                assert({nombre: existeUsuario.nombre, password: existeUsuario.password},{nombre: usuario,password:password});
            }else{
                assert.fail('No se encontro el usuario');
            }
        })
        it('Inicio de sesion erronea',async function(){
            let usuario = "Administrador";
            let password = "123457";
            const existeUsuario = await Usuario.findOne({usuario: usuario});            
            assert.notEqual(existeUsuario.password,password);            
        })

        it('Revision de usuario tipo operador',async function(){
            let usuario = "Operador";
            let rol = "operador";
            const existeUsuario = await Usuario.findOne({usuario: usuario});            
            assert.equal(existeUsuario.rol,rol);            
        })
    });
}