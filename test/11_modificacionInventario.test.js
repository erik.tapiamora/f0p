var assert = require("assert");
const Relacion_Libro_Sucursal = require("../model/Relacion_Libro_Sucursal");
const Sucursal = require("../model/Sucursal");
const Libro = require("../model/Libro");

module.exports= function (){
    describe('11 - Modificar cantidad de libros en una sucursal',async function(){
        it('Obtiene un libro y lo agregas al inventario de la sucursal 2',async function(){
            let libro = await Libro.findOne({id: 1});
            let sucursal_inventario = await Relacion_Libro_Sucursal.findOneAndUpdate(
                {
                    libro_id:libro.id,
                    sucursal_id:2
                },
                {
                    cantidad: 7
                });
            sucursal_inventario = await Relacion_Libro_Sucursal.findOne(
                {
                    libro_id: libro.id,
                    sucursal_id: 2
                }
            )
           
            assert.equal(sucursal_inventario.cantidad,7);
        });
    });
}