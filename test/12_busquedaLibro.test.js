var assert = require("assert");
const Libro = require("../model/Libro");
const Relacion_Libro_Sucursal = require("../model/Relacion_Libro_Sucursal");


module.exports= function (){
    describe('12 - Proceso de busqueda de un libro por nombre y descubrir en que sucursal se encuentra',async function(){        
        it('Se ingresa la información del libro y se obtienen las sucursales con stock',async function(){
            const libro = await Libro.findOne({                
                titulo: "Relatos fantásticos",                      
            })
            const sucursales_disponibles = await Relacion_Libro_Sucursal.find({libro_id: libro.id});            
            
            assert.equal(sucursales_disponibles.length>0,true)
        })

    
    });
}