var assert = require("assert");
const Sucursal = require("../model/Sucursal");

module.exports= function (){
    describe('2 -Ver listado de sucursales',async function(){
        it('Obtiene un listado de sucursales',async function(){
            let sucursales = await Sucursal.find();
            if(sucursales){
                assert(true);
            }
            else{
                assert.fail('No existe un listado de sucursales');
            }
        });

        it('Valida que el total de sucursales existentes es 3',async function(){
            let sucursales = await Sucursal.find();
            assert.equal(sucursales.length,3);            
        });
    });
}