var assert = require("assert");
const Categoria = require("../model/Categoria");

module.exports= function (){
    describe('8 - Proceso de baja de categoria',async function(){
        it('Se da de baja la categoriaa y se valida existe el registro',async function(){
            const registroAEliminar = await Categoria.findOne({
                id:4
            });
            await registroAEliminar.remove();
            const existeRegistro = await Categoria.findOne({id:4});
            assert.equal(existeRegistro,null);
        })

        it('Se verifica sean ahora 3 categorias',async function(){

            const listadoRegistro = await Categoria.find();
            assert.equal(listadoRegistro.length,3);
        })
    });
}