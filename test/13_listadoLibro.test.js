var assert = require("assert");
const Libro = require("../model/Libro");

module.exports= function (){
    describe('13 - Ver listado de libros',async function(){
        it('Obtiene un listado de Libros',async function(){
            let libros = await Libro.find();
            if(libros){
                assert(true);
            }
            else{
                assert.fail('No existe un listado de categorias');
            }
        });

        it('Valida que el total de categorias existentes es 9',async function(){
            let libros = await Libro.find();
            
            assert.equal(libros.length,9);            
        });
    });
}