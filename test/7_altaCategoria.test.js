var assert = require("assert");
const Categoria = require("../model/Categoria");


module.exports= function (){
    describe('7 - Proceso de alta de nueva categoria',async function(){
        it('Se da de alta la categoria y se valida existe el registro',async function(){
            const nuevoRegistro = await Categoria.create({
                id:4,
                nombre: "Misterio",       
               
            })
            const existeRegistro = await Categoria.findOne({_id:nuevoRegistro._id});
            assert.equal(nuevoRegistro.id,existeRegistro.id);
        })

        it('Se verifica sean ahora 4 categorias',async function(){

            const listadoRegistro = await Categoria.find();
            assert.equal(listadoRegistro.length,4);
        })
    });
}