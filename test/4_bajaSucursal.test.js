var assert = require("assert");
const Sucursal = require("../model/Sucursal");

module.exports= function (){
    describe('4 - Proceso de baja de sucursal',async function(){
        it('Se da de baja la sucursal y se valida existe el registro',async function(){
            const registroAEliminar = await Sucursal.findOne({
                id:4
            });
            await registroAEliminar.remove();
            const existeRegistro = await Sucursal.findOne({id:4});
            assert.equal(existeRegistro,null);
        })

        it('Se verifica sean ahora 3 sucursales',async function(){

            const listadoRegistro = await Sucursal.find();
            assert.equal(listadoRegistro.length,3);
        })
    });
}