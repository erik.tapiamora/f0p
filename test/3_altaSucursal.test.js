var assert = require("assert");
const Sucursal = require("../model/Sucursal");


module.exports= function (){
    describe('3 - Proceso de alta de nueva sucursal',async function(){
        it('Se da de alta la sucursal y se valida existe el registro',async function(){
            const nuevoRegistro = await Sucursal.create({
                id:4,
                nombre: "Libreria Plaza Universidad",       
                direccion: "Limon Padilla",       
                ciudad: "Tijuana, Baja California",
                codigo_postal: 22215,      
            })
            const existeRegistro = await Sucursal.findOne({_id:nuevoRegistro._id});
            assert.equal(nuevoRegistro.id,existeRegistro.id);
        })

        it('Se verifica sean ahora 4 sucursales',async function(){

            const listadoRegistro = await Sucursal.find();
            assert.equal(listadoRegistro.length,4);
        })
    });
}