var assert = require("assert");
const Categoria = require("../model/Categoria");

module.exports= function (){
    describe('9 - Proceso de modificacion de categoria',async function(){
        it('Se obtiene el registro, edita y valida se actualizará en DB',async function(){
            const nuevoValor = 'Suspenso';
            const registroAModificar = await Categoria.findOneAndUpdate({
                id:3
            },
            {
                nombre: nuevoValor,
            });
            const registroEditado = await Categoria.findOne({
                id:3
            });
            assert.equal(registroEditado.nombre,nuevoValor);
        })
    });
}