const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;

const SchemaRelacionLibroSucursal = new mongoose.Schema({

    sucursal_id : Number,
    libro_id : Number,
    cantidad: Number,
});

module.exports =   mongoose.model('RelacionLibroSucursal',SchemaRelacionLibroSucursal,'relaciones_libros_sucursales');