const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;


const SchemaMovimiento = new mongoose.Schema({
    libro_Id : Number,
    sucursal_Id: Number,
    usuario_Id: Number,
    cantidad : Number,
    fecha: Date,
    tipo: {
        type: String,
        enum: ['Compra','Venta','Devolucion','Traslado']        
    },
});



module.exports = mongoose.model('Movimiento',SchemaMovimiento,'movimientos');