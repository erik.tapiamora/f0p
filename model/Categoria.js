const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;



const SchemaCategoria = new mongoose.Schema({
    id: Number,
    nombre: String,    
});



module.exports = mongoose.model('Categoria',SchemaCategoria,'categorias');