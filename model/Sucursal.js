const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;

const SchemaSucursal = new mongoose.Schema({
    id: Number,
    nombre: String,
    direccion: String,
    ciudad: String,
    codigo_postal: Number,
});





module.exports = mongoose.model('Sucursal',SchemaSucursal,'sucursales');