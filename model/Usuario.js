const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;

const SchemaUsuario = new mongoose.Schema({
    id: Number,
    nombre: String,       
    apellido: String,       
    edad: Number,
    correo: String,
    usuario: String,
    contraseña: String,
    rol: {
        type: String,
        enum : ['administrador','supervisor','operador','cliente'],
        default: 'cliente'
    },
});



module.exports = mongoose.model('Usuario',SchemaUsuario,'usuarios');