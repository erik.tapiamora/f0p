const { ObjectID } = require("bson");
const { default: mongoose } = require("mongoose");
const {Schema} = mongoose;

const SchemaLibro = new mongoose.Schema({
    id: Number,
    codigo_interno: String,
    titulo: String,
    editorial: String,
    autor: String,    
    categoria_id: Number,

});

module.exports =  mongoose.model('Libro',SchemaLibro,'libros');