const Categoria = require("../model/Categoria");

module.exports = async function GenerarCategorias() {
  await Categoria.create({
    id: 1,
    nombre: "Aventura",
  });

  await Categoria.create({
    id: 2,
    nombre: "Acción",
  });
  await Categoria.create({
    id: 3,
    nombre: "Terror",
  });
 

  console.log("Categorias generadas");
};
