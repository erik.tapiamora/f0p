const Usuario = require("../model/Usuario");

module.exports = async function GenerarUsuarios() {
    await Usuario.create({
        id:1,
        nombre: "Erik",       
        apellido: "Tapia",       
        edad: "25",
        correo: "admin@libreria.com",
        usuario: "Administrador",
        contraseña: "123456",
        rol: "administrador"
    });    

    await Usuario.create({
        id:2,
        nombre: "Moises",       
        apellido: "Avila",       
        edad: "23",
        correo: "operador@libreria.com",
        usuario: "Operador",
        contraseña: "123456",
        rol: "operador"
    });    

    await Usuario.create({
        id:3,
        nombre: "Misael",       
        apellido: "Sanchez",       
        edad: "24",
        correo: "supervisor@libreria.com",
        usuario: "Supervisor",
        contraseña: "123456",
        rol: "supervisor"
    });   

    await Usuario.create({
        id:4,
        nombre: "Roberto",       
        apellido: "Salazar",       
        edad: "26",
        correo: "cliente@gmail.com",
        usuario: "Cliente",
        contraseña: "123456",
        rol: "cliente"
    });  
    console.log("Usuarios generados");
}