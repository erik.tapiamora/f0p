const Libro = require("../model/Libro");

module.exports = async function GenerarLibro() {
  await Libro.create({
    id: 1,
    codigo_interno: "A001",
    titulo: "Relatos fantásticos",
    editorial: "Alianza Editorial",
    autor: "Carlos Garcia Gual",
    categoria_id: 1,
  });

  await Libro.create({
    id: 2,
    codigo_interno: "A002",
    titulo: "Los tres mosqueteros",
    editorial: "Penguin Clasicos",
    autor: "Alexandre Dumas",
    categoria_id: 1,
  });

  await Libro.create({
    id: 3,
    codigo_interno: "A003",
    titulo: "La isla del tesoro",
    editorial: "Penguin Clasicos",
    autor: "Jordi Beltran Ferrer",
    categoria_id: 1,
  });

  await Libro.create({
    id: 4,
    codigo_interno: "B001",
    titulo: "El corsario negro",
    editorial: "Alianza Editorial",
    autor: "Emili Salgari",
    categoria_id: 2,
  });

  await Libro.create({
    id: 5,
    codigo_interno: "B002",
    titulo: "Moby Dick",
    editorial: "Dover Thrift",
    autor: "Herman Melville",
    categoria_id: 2,
  });

  await Libro.create({
    id: 6,
    codigo_interno: "B003",
    titulo: "Origen",
    editorial: "Planeta",
    autor: "Dan Brown",
    categoria_id: 2,
  });

  await Libro.create({
    id: 7,
    codigo_interno: "C001",
    titulo: "It",
    editorial: "Delbolsillo",
    autor: "Sthephen King",
    categoria_id: 3,
  });

  await Libro.create({
    id: 8,
    codigo_interno: "C002",
    titulo: "La Tienda",
    editorial: "Delbolsillo",
    autor: "Sthephen King",
    categoria_id: 3,
  });

  await Libro.create({
    id: 9,
    codigo_interno: "C003",
    titulo: "El resplandor",
    editorial: "Delbolsillo",
    autor: "Sthephen King",
    categoria_id: 3,
  });

  console.log("Libros generados");
};
