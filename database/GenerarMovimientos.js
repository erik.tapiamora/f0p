const Movimiento = require("../model/Movimiento");

module.exports = async function GenerarMovimientos() {
    await Movimiento.create({
        libro_Id: 1,
        sucursal_Id: 1,
        usuario_Id: 1,
        cantidad: 1,
        fecha: new Date(),
        tipo: 'Compra'        
    });    

    await Movimiento.create({
        libro_Id: 4,
        sucursal_Id: 3,
        usuario_Id: 2,
        cantidad: 2,
        fecha: new Date(),
        tipo: 'Venta'        
    });    

    await Movimiento.create({
        libro_Id: 5,
        sucursal_Id: 3,
        usuario_Id: 2,
        cantidad: 5,
        fecha: new Date(),
        tipo: 'Traslado'        
    }); 
    console.log("Movimientos generados");
}