const Relacion_Libro_Sucursal = require("../model/Relacion_Libro_Sucursal");

module.exports = async function GenerarUsuarios() {
    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 1,       
        libro_id: 1,       
        cantidad: 10,
    });    

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 2,       
        libro_id: 1,       
        cantidad: 6,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 3,       
        libro_id: 1,       
        cantidad: 4,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 1,       
        libro_id: 2,       
        cantidad: 7,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 1,       
        libro_id: 3,       
        cantidad: 5,
    });   


    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 2,       
        libro_id: 4,       
        cantidad: 7,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 2,       
        libro_id: 5,       
        cantidad: 5,
    }); 
    
    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 2,       
        libro_id: 6,       
        cantidad: 13,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 3,       
        libro_id: 7,       
        cantidad: 11,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 3,       
        libro_id: 8,       
        cantidad: 7,
    });   

    await Relacion_Libro_Sucursal.create({       
        sucursal_id: 3,       
        libro_id: 9,       
        cantidad: 16,
    });   
    console.log("Relacion de libros y sucursales generados");
}