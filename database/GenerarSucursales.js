const Sucursal = require("../model/Sucursal");

module.exports = async function GenerarSucursal() {
    await Sucursal.create({
        id:1,
        nombre: "Libreria Plaza Paseo 2000",       
        direccion: "Blvd 2000",       
        ciudad: "Tijuana, Baja California",
        codigo_postal: 22235,      
    });    

    await Sucursal.create({
        id:2,
        nombre: "Libreria Macroplaza 2000",       
        direccion: "Blvd Insurgentes",       
        ciudad: "Tijuana, Baja California",
        codigo_postal: 22335,      
    });    

    await Sucursal.create({
        id:3,
        nombre: "Libreria Plaza Sendero",       
        direccion: "El Refugio",       
        ciudad: "Tijuana, Baja California",
        codigo_postal: 22237,      
    });    
   
    console.log("Sucursales Generadas");
}